import gensim
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv

f = open("1.csv")
data = []
f.readline()
for line in f.readlines():
    string = line.split(',')
    data.append(dict(time=string[0], source=string[1], ip=string[2], connection=string[3], department=string[4],
                     login=string[5], device=string[6], service=string[7], action=string[8]))
sentences = [[a["service"] for a in data], [b["ip"] for b in data]]
model = gensim.models.Word2Vec(sentences, min_count=1, size=150)

vocab = list(model.wv.vocab)
dictionary = {}
i = 0
for v in vocab:
    dictionary[i] = v
    i += 1
X = model[vocab]
tsne = TSNE(n_components=2)
X_tsne = tsne.fit_transform(X)
df = pd.concat([pd.DataFrame(X_tsne),
                pd.Series(list(dictionary.keys()))],
               axis=1)
df.columns = ['x', 'y', 'word']
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.scatter(df['x'], df['y'])
for i, txt in enumerate(df['word']):
    ax.annotate(txt, (df['x'].iloc[i], df['y'].iloc[i]))
plt.show()
print(model.similarity(dictionary[31], dictionary[38]))
